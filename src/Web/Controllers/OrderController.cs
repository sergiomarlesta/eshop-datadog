﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.eShopWeb.Web.Features.MyOrders;
using Microsoft.eShopWeb.Web.Features.OrderDetails;
using Datadog.Trace;


namespace Microsoft.eShopWeb.Web.Controllers;

[ApiExplorerSettings(IgnoreApi = true)]
[Authorize] // Controllers that mainly require Authorization still use Controller/View; other pages use Pages
[Route("[controller]/[action]")]
public class OrderController : Controller
{
    private readonly IMediator _mediator;
    private readonly Microsoft.Extensions.Logging.ILogger<OrderController> _logger;


    public OrderController(IMediator mediator
        , ILogger<OrderController> logger
        )
    {
        _mediator = mediator;
        _logger = logger;

    }

    [HttpGet]
    public async Task<IActionResult> MyOrders()
    {
        var viewModel = await _mediator.Send(new GetMyOrders(User.Identity.Name));

        return View(viewModel);
    }

    [HttpGet("{orderId}")]
    public async Task<IActionResult> Detail(int orderId)
    {
        
        _logger.LogWarning($"YY|||||ENV|{CorrelationIdentifier.Env}||||||");
        _logger.LogWarning($"YY|||||SERVICE|{CorrelationIdentifier.Service}||||||");
        _logger.LogWarning($"YY|||||VERSION|{CorrelationIdentifier.Version}||||||");
        _logger.LogWarning($"YY|||||TRACEID|{CorrelationIdentifier.TraceId.ToString()}||||||");
        _logger.LogWarning($"YY|||||SPANID |{CorrelationIdentifier.SpanId.ToString()}||||||");

        _logger.LogInformation($"YY|||||ENV|{CorrelationIdentifier.Env}|||||SERVICE|{CorrelationIdentifier.Service}|||||VERSION|{CorrelationIdentifier.Version}|||||TRACEID|{CorrelationIdentifier.TraceId.ToString()}|||||SPANID |{CorrelationIdentifier.SpanId.ToString()}||||||");
        _logger.LogWarning($"YY|||||ENV|{CorrelationIdentifier.Env}|||||SERVICE|{CorrelationIdentifier.Service}|||||VERSION|{CorrelationIdentifier.Version}|||||TRACEID|{CorrelationIdentifier.TraceId.ToString()}|||||SPANID |{CorrelationIdentifier.SpanId.ToString()}||||||");
        using (_logger.BeginScope(new Dictionary<string, object>
        {
            {"dd.env", CorrelationIdentifier.Env},
            {"dd.service", CorrelationIdentifier.Service},
            {"dd.version", CorrelationIdentifier.Version},
            {"dd.trace_id", CorrelationIdentifier.TraceId.ToString()},
            {"dd.span_id", CorrelationIdentifier.SpanId.ToString()},
            {"dd.source", "csharp"},
            {"Testing", "test"},

        }))
        {
            _logger.LogInformation($"XX**------Retrieving order with id :{orderId}.-----------");
            _logger.LogWarning($"XX**********Retrieving order with id :{orderId}.**********");
            _logger.LogInformation($"XX**|||||ENV|{CorrelationIdentifier.Env}|||||SERVICE|{CorrelationIdentifier.Service}|||||VERSION|{CorrelationIdentifier.Version}|||||TRACEID|{CorrelationIdentifier.TraceId.ToString()}|||||SPANID |{CorrelationIdentifier.SpanId.ToString()}||||||");
            _logger.LogWarning($"XX**|||||ENV|{CorrelationIdentifier.Env}|||||SERVICE|{CorrelationIdentifier.Service}|||||VERSION|{CorrelationIdentifier.Version}|||||TRACEID|{CorrelationIdentifier.TraceId.ToString()}|||||SPANID |{CorrelationIdentifier.SpanId.ToString()}||||||");
            _logger.LogError("XXTest de error");

        }

        var viewModel = await _mediator.Send(new GetOrderDetails(User.Identity.Name, orderId));

        if (viewModel == null)
        {
            return BadRequest("No such order found for this user.");
        }

        return View(viewModel);
    }
}
